# SMTP Server Python (asyncio)

RS_SMTPD_HANDLER=aiosmtpd.handlers.Debugging python -m rs_smtpd.server

RS_SMTPD_HANDLER=aiosmtpd.handlers.Mailbox

RS_SMTPD_HANDLER=aiosmtpd.handlers.Sink

export RS_SMTPD_HANDLER=rs_smtpd.handlers.rest.Handler
export RS_SMTPD_HANDLER_SETTINGS=rs_smtpd.handlers.rest.LocalSettings 
python -m rs_smtpd.server

