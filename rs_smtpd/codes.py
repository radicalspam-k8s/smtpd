"""
/home/srault/code/radicalspam-k8s/.direnv/python-3.8.5/lib/python3.8/site-packages/aiosmtpd/testing/statuscodes.py

220	Premier code envoyé par le serveur lorsque la connexion s'est effectuée avec succès.
250	Confirmation de commande acceptée.
354	Réponse à la commande DATA. Le serveur attend les données du corps du message. Le client indique la fin du message par un point seul sur une ligne : <CR><LF>.<CR><LF>
421	Échec temporaire au niveau de la connexion. Il se peut que le serveur soit surchargé, qu'il limite le nombre de connexions en provenance d'une même adresse IP ou que le service soit indisponible.
    421 4.7.0 {command} sent too many times
452	Échec temporaire : nombre de destinataires maximum atteint.
550	Échec permanent. La boîte aux lettres n'existe pas ou l'adresse du destinataire est invalide.
554	Échec permanent au niveau de la connexion : utilisé à la place du code 220 pour les hôtes sur liste noire.
"""
