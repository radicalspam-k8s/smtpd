from typing import Optional, Dict, List, Tuple, Union, Type, Any
import logging
import asyncio

"""
FIXME: uvloop and tls

import uvloop
#asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

  File "/home/srault/code/radicalspam-k8s/.direnv/python-3.8.5/lib/python3.8/site-packages/aiosmtpd/smtp.py", line 859, in smtp_STARTTLS
    self._original_transport._protocol = self._tls_protocol
AttributeError: 'uvloop.loop.TCPTransport' object has no attribute '_protocol'
"""

from aioprometheus import Service, formats, count_exceptions, timer

from aiosmtpd.controller import Controller as OriginalController
from aiosmtpd.smtp import SMTP as OriginalSMTP, syntax

from .models import Envelope
from .config import get_settings, Settings
from .utils import utcnow_for_json
from . import metrics

logger = logging.getLogger(__name__)


class SMTP(OriginalSMTP):
    def _create_envelope(self) -> Envelope:
        """Overload for use my Envelope class"""
        return Envelope(created=utcnow_for_json())

    async def smtp_DATA(self, arg: str):
        """Overload for add Session datas in Envelope"""

        if await self.check_helo_needed():
            return
        if await self.check_auth_needed("DATA"):
            return
        if not self.envelope.rcpt_tos:
            await self.push("503 Error: need RCPT command")
            return
        if arg:
            await self.push("501 Syntax: DATA")
            return

        self.envelope.client_address = self.session.peer[0]
        self.envelope.server_hostname = self.hostname

        # FIXME: attention, c'est le helo name
        self.envelope.host_name = self.session.host_name
        self.envelope.helo_name = self.session.host_name

        # TODO: call handler before data ?

        await super().smtp_DATA(arg)

        metrics.RECEIVE_MAIL_COUNT.inc({})


class Controller(OriginalController):
    def factory(self) -> SMTP:
        """Overload for use my SMTP class"""
        return SMTP(self.handler, **self.SMTP_kwargs)


class Server:
    def __init__(self, settings: Settings, handler: Type[Any], loop=None):

        self.settings = settings
        self.handler = handler
        self._server = Controller(
            self.handler,
            hostname=settings.bind_host,
            port=settings.bind_port,
            **settings.smtp_parameters(),
        )

        self.loop = loop

    async def monitoring_setup(self):
        svr = Service(loop=self.loop)
        svr.register(metrics.RECEIVE_MAIL_COUNT)
        svr.register(metrics.HANDLER_EXCEPTIONS)
        svr.register(metrics.HANDLER_PROCESSING_SECONDS)
        await svr.start(port=self.settings.prometheus_port)

    def start(self):
        self._server.start()

    def stop(self):
        self._server.start()

    def reload(self):
        pass

    def pause(self):
        # TODO: reject all mail with 421 code
        pass


async def amain(loop, settings: Settings):

    handler_settings = dict()
    if settings.handler_settings:
        handler_settings = settings.handler_settings().dict()

    server = Server(
        settings=settings, handler=settings.handler(**handler_settings), loop=loop
    )

    server.start()

    if settings.prometheus_enable:
        await server.monitoring_setup()

    handler_name = (
        f"{server.handler.__class__.__module__}.{server.handler.__class__.__name__}"
    )
    # TODO: add mx name et autre params importants
    msg = f"START host[{server._server.hostname}] - port[{server._server.port}] - handler[{handler_name}]"
    logger.info(msg)


if __name__ == "__main__":

    settings = get_settings()
    logging_level = logging.INFO
    if settings.debug:
        logging_level = logging.DEBUG

    logging.basicConfig(
        level=logging_level,
        format="line:%(lineno)d - %(asctime)s %(name)s: [%(levelname)s] - [%(process)d] - [%(module)s] - %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )

    loop = asyncio.get_event_loop()
    loop.create_task(amain(loop=loop, settings=settings))
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
