import zlib
import uuid
import hashlib

import arrow


def utcnow():
    return arrow.utcnow().datetime


def utcnow_for_json():
    return arrow.utcnow().for_json()


def generate_key():
    """Génère un ID unique de 64 caractères"""
    new_uuid = str(uuid.uuid4()).encode()
    return str(hashlib.sha1(new_uuid).hexdigest())


def compress(data):
    if not isinstance(data, bytes):
        # return base64.b64encode(zlib.compress(data.encode()))
        return zlib.compress(data.encode())
    else:
        # return base64.b64encode(zlib.compress(data))
        return zlib.compress(data)


def uncompress(data):
    # return zlib.decompress(base64.b64decode(data.decode()))
    return zlib.decompress(data)  # .decode())
