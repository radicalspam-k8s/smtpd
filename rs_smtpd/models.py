from datetime import datetime
from typing import Optional, Dict, List, Tuple, Union
from enum import Enum

from pydantic import BaseModel, Field

from .utils import generate_key


class Envelope(BaseModel):

    uid: str = generate_key()

    created: str  # = utcnow_for_json()

    peer: Tuple = None  # ('::1', 57154, 0, 0)
    # ssl = None
    client_address: str = None
    host_name: str = None
    helo_name: str = None

    server_hostname: str = None

    mail_from: str = None
    mail_options: List[str] = []

    smtp_utf8: bool = False

    content: Union[None, bytes, str] = None
    original_content: bytes = None

    rcpt_tos: List[str] = []
    rcpt_options: List[str] = []

    forward_attrs: Dict = {}

    # name: str = Field(
    #     None, title="Name of project", max_length=300, example="storage-account"
    # )
    # template: Dict
    # json_schema: Dict
    # metas: Optional[Dict]

    # class Config:
    #     schema_extra = {
    #         "example": {
    #             "name": "storage-account2",
    #             "template": {},
    #             "json_schema": {},
    #             "metas": None,
    #         }
    #     }


class ProjectStatusEnum(str, Enum):
    SUCCESS = "SUCCESS"
    ERROR = "ERROR"
    UNKNOW = "UNKNOW"


class ProjectParametersForRG(BaseModel):
    name: str = Field(
        None,
        title="Name of project",
        max_length=300,
        example="storage-account",
        description="ARM Project (Directory name of ARM template)",
    )

    subscription_id: str = Field(
        None, title="Subscription ID", example="00000000-0000-0000-0000-000000000000"
    )

    rg_name: str = Field(None, title="Resource Group Name", example="rg1")

    payload: Dict = Field(
        None, title="ARM Parameters", example={"storageAccountName": "mystorage"}
    )

    deployment_id: Optional[str] = Field(
        None, title="Deployment ID ", example="123456789ABCDEF"
    )

    class Config:
        schema_extra = {
            "example": {
                "name": "storage-account",
                "subscription_id": "00000000-0000-0000-0000-000000000000",
                "rg_name": "myrg",
                "payload": {"storageAccountName": "mystorage"},
            }
        }


class ProjectParametersForRGResponse(BaseModel):
    name: str = Field(
        None,
        title="Name of project",
        max_length=300,
        example="storage-account",
        description="ARM Project (Directory name of ARM template)",
    )

    deployment_id: str = Field(None, title="Deployment ID ", example="123456789ABCDEF")

    status: ProjectStatusEnum = Field(
        None,
        title="Deployment Status",
    )

    status_code: int = Field(None, title="Deployment response code")

    result: Dict = Field(
        None,
        title="Deployment output",
        # example={}
    )
