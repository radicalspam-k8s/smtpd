import asyncio
from pprint import pprint
from typing import List, Dict, Set
import logging
import itertools

from async_timeout import timeout
import httpx

from aioprometheus import count_exceptions, timer

# from ..config import get_settings, Settings
from rs_smtpd import metrics

# import mailparser
# from flanker import mime
# from flanker.mime.create import from_string
# from flanker.mime.message.fallback.create import from_string as recover

logger = logging.getLogger(__name__)

from pydantic import BaseSettings, HttpUrl


class LocalSettings(BaseSettings):
    remote_urls: Set[HttpUrl]
    timeout: int = 30
    sleep_retry: int = 5
    # retries: int = 0

    class Config:
        env_file = ".env"
        env_prefix = "rs_smtpd_rest_"
        validate_assignment = True


class Handler:

    # Important à voir: https://aiosmtpd.readthedocs.io/en/latest/proxyprotocol.html
    # https://www.haproxy.com/blog/efficient-smtp-relay-infrastructure-with-postfix-and-load-balancers/

    def __init__(
        self, remote_urls: Set[HttpUrl], timeout: int = 30, sleep_retry: int = 5
    ):
        self.remote_urls = [str(item) for item in remote_urls]
        self.urls = itertools.cycle(self.remote_urls)
        self.timeout = timeout
        self.sleep_retry = sleep_retry

    @count_exceptions(metrics.HANDLER_EXCEPTIONS, {"handler": "data"})
    async def _post_message(self, url: str, message: Dict) -> str:

        # TODO: compress + base64 pour réduire taille transfert ? (avec header spécifique)
        # TODO: timeout, retry, auth, ...

        async with httpx.AsyncClient() as client:
            response = await client.post(url, json=message, timeout=60)

        # TODO: response.raise_for_status()

        return response.json()["smtp_response"]

    @timer(metrics.HANDLER_PROCESSING_SECONDS)
    async def handle_DATA(self, server, session, envelope) -> str:
        """Handler called after receive all datas"""

        try:
            async with timeout(self.timeout):
                while True:
                    try:
                        msg = await self._post_message(
                            next(self.urls), envelope.dict(exclude={"original_content"})
                        )
                        return msg
                    except Exception as err:
                        msg = str(err)
                        logger.error(msg)

                    await asyncio.sleep(self.sleep_retry)
        except asyncio.TimeoutError:
            msg = f"timeout error for urls {self.remote_urls}. Max timeout: {self.timeout} sec"
            logger.error(msg)
            return "450 Service currently unavailable"
