import socket
import uuid

# from aioprometheus import Counter, Summary, Service, count_exceptions, timer
from aioprometheus import Counter, Gauge, Histogram, Service, Summary, formats

# from prometheus_client import Info

from .version import __version__

const_labels = {"host": socket.gethostname(), "app": "rs-smtpd", "version": __version__}

RECEIVE_MAIL_COUNT = Counter(
    "receive_mail_count", "Number of mail receive", const_labels=const_labels
)
HANDLER_EXCEPTIONS = Counter(
    "handler_exceptions", "Number of exceptions in handler", const_labels=const_labels
)
HANDLER_PROCESSING_SECONDS = Summary(
    "request_processing_seconds",
    "Time spent processing request",
    const_labels=const_labels,
)

"""
psutil !
    https://github.com/claws/aioprometheus/blob/master/examples/app-example.py

        self.ram_metric = Gauge(
            "memory_usage_bytes", "Memory usage in bytes.", const_labels=const_labels
        )    
        self.cpu_metric = Gauge(
            "cpu_usage_percent", "CPU usage percent.", const_labels=const_labels
        )        
        self.ram_metric.set({"type": "virtual"}, psutil.virtual_memory().used)
        self.ram_metric.set({"type": "swap"}, psutil.swap_memory().used)
        for c, p in enumerate(psutil.cpu_percent(interval=1, percpu=True)):
            self.cpu_metric.set({"core": c}, p)


Queue mail count
Current Session count

Number of mail receive
Number of mail reject
Number of mail reject 5xx
Mail size average
Mail size max
Handler Exception count

Il faudrait tous ces compteurs en version last_24H ?

"""


"""

events_counter = Counter(
    "events", "Number of events.", const_labels={"host": socket.gethostname()}
)
svr.register(events_counter)
events_counter.inc({"kind": "timer_expiry"})

ram_metric = Gauge("memory_usage_bytes", "Memory usage in bytes.")
ram_metric.set({'type': "virtual"}, 100)

http_access =  Summary("http_access_time", "HTTP access time")
http_access.observe({'time': '/static'}, 3.142)

http_access =  Histogram("http_access_time", "HTTP access time")
http_access.observe({'time': '/static'}, 3.142)


const_labels = {
    "host": socket.gethostname(),
    "app": f"{self.__class__.__name__}-{uuid.uuid4().hex}",
}
self.requests_metric = Counter(
    "requests", "Number of requests.", const_labels=const_labels
)
"""

"""
curl http://127.0.0.1:8000/metrics
# HELP handler_exceptions Number of exceptions in handler
# TYPE handler_exceptions counter
# HELP request_processing_seconds Time spent processing request
# TYPE request_processing_seconds summary
request_processing_seconds{quantile="0.5"} 0.002489399994374253
request_processing_seconds{quantile="0.9"} 0.002489399994374253
request_processing_seconds{quantile="0.99"} 0.002489399994374253
request_processing_seconds_count 1
request_processing_seconds_sum 0.002489399994374253
"""
