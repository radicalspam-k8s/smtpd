from functools import lru_cache
from pathlib import Path
from typing import Dict, Any, List, Type, Set
import ssl
import socket

from pydantic import BaseSettings, PyObject

from .version import __version__


class Settings(BaseSettings):
    app_version: str = __version__
    debug: bool = False
    bind_host: str = "0.0.0.0"
    bind_port: int = 2500
    prometheus_enable: bool = True
    prometheus_port: int = 8000
    handler: PyObject  # ex: 'rs_smtpd.handlers.RestHandler' or RS_SMTPD_HANDLER=rs_smtpd.handlers.RestHandler
    handler_settings: PyObject = None

    ssl_cert_filepath: str = None
    ssl_key_filepath: str = None
    require_starttls: bool = False

    hostname: str = socket.gethostname()
    data_size_limit: int = 2 ** 25  # 32 Mo
    enable_SMTPUTF8: bool = True
    decode_data: bool = True
    proxy_protocol_timeout: int = (
        None  # si != None, active le proxy protocol pour haproxy ou autre
    )

    """

    Paramètres de SMTP qui peuvent être passé au Controleur
        ident=None,
        tls_context: Optional[ssl.SSLContext] = None,
        require_starttls=False,
        timeout=300,
        auth_required=False,
        auth_require_tls=True,
        auth_exclude_mechanism: Optional[Iterable[str]] = None,
        auth_callback: AuthCallbackType = None,
        command_call_limit: Union[int, Dict[str, int], None] = None,
        authenticator: AuthenticatorType = None,
        proxy_protocol_timeout: Optional[Union[int, float]] = None,
    """

    class Config:
        env_file = ".env"
        env_prefix = "rs_smtpd_"
        validate_assignment = True

    def smtp_parameters(self):
        data = dict(
            data_size_limit=self.data_size_limit,
            enable_SMTPUTF8=self.enable_SMTPUTF8,
            decode_data=self.decode_data,
            server_hostname=self.hostname,
            require_starttls=False,
        )

        if self.ssl_cert_filepath and self.ssl_key_filepath:
            context = ssl.create_default_context(
                ssl.Purpose.CLIENT_AUTH
            )  # , cafile=None, capath=None, cadata=None
            # context.check_hostname = False
            context.load_cert_chain(self.ssl_cert_filepath, self.ssl_key_filepath)
            data["tls_context"] = context

        return data


@lru_cache()
def get_settings():
    return Settings()
