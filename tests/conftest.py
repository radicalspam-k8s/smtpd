"""
from pkg_resources import resource_filename
SERVER_CRT = resource_filename("aiosmtpd.tests.certs", "server.crt")

# helper.py
# pytest.register_assert_rewrite("pytest_foo.helper")

pytest_plugins = "fastmep_tasks.pytest.plugin"
"""
pytest_plugins = "aiosmtpd.tests.conftest"