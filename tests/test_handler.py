#from aiosmtpd.testing.statuscodes import SMTP_STATUS_CODES as S
import smtplib
from smtplib import SMTP as Client
import traceback

import pytest

from rs_smtpd.handlers.rest import Handler as OriginalHandler
from rs_smtpd.server import Controller as BaseController

pytest_plugins = "aiosmtpd.tests.conftest"

pytestmark = pytest.mark.asyncio

# def test_say(event_loop):
#     expected = 'This should fail!'
#     assert expected == event_loop.run_until_complete(say('Hello!', 0))

class Controller(BaseController):
    pass

class Handler(OriginalHandler):
    pass
    # def __init__(self) -> None:
    #     super().__init__()
    #     self.messages = []

@pytest.fixture
def controller(request, unused_tcp_port_factory, event_loop):
    handler = Handler()
    port = unused_tcp_port_factory()
    controller = Controller(handler, 
        #loop=event_loop, 
        hostname='127.0.0.1', port=port)
    controller.start()

    def _finalize():
        controller.stop()
    request.addfinalizer(_finalize)

    yield controller

#@pytest.mark.client_data
def test_smtp_sent(controller):
    with Client(controller.hostname, controller.port) as client:
        r = client.ehlo("example.com")
        print('!!! ehlo : ', r[1].decode().split('\n')) 
        # ['LAPTOP-P06203SE.localdomain', 'SIZE 33554432', '8BITMIME', 'SMTPUTF8', 'HELP']
        """
        xforward = {
            'ADDR': '192.168.1.1',
            'NAME': 'mail.local.net',
            'HELO': 'local.net',
        }
        (code, msg) = s.docmd('XFORWARD', 'ADDR=%(ADDR)s NAME=%(NAME)s HELO=%(HELO)s' % xforward)
        result['xforward'] = [code, msg]
        if code != 250: return result
        """
        client.sendmail('andy@love.com', ['bob@hate.com'], """\
        Date: 17/05/2017,2:18
        From: andy@love.com
        To: bob@hate.com
        Subject: A test

        testing
        """)

