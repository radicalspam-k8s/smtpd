ARG PYTHON_VERSION=3.8-alpine3.10

FROM python:${PYTHON_VERSION}

LABEL maintainer="stephane.rault@radicalspam.org"

ENV PYTHONUNBUFFERED=1

ENV RS_SMTPD_BIND_HOST=0.0.0.0
ENV RS_SMTPD_BIND_PORT=2500

ENV RS_SMTPD_HOSTNAME=mx.openkass.org

ENV RS_SMTPD_PROMETHEUS_ENABLE=true

ENV RS_SMTPD_HANDLER=rs_smtpd.handlers.rest.Handler
ENV RS_SMTPD_HANDLER_SETTINGS=rs_smtpd.handlers.rest.LocalSettings

COPY . /app

WORKDIR /app

RUN set -ex \
    && apk add --no-cache --virtual .build-deps gcc python3-dev build-base libffi-dev libressl-dev linux-headers \    
    && pip install --no-cache-dir -U pip==21.0.1 wheel==0.36.2 \
    && CRYPTOGRAPHY_DONT_BUILD_RUST=1 pip install --no-cache-dir -r requirements.txt \
    && apk del .build-deps \
    && rm -rf /tmp/* /var/cache/apk/* \
    && rm -rf ~/.cache/pip

EXPOSE ${RS_SMTPD_PORT}

EXPOSE ${RS_SMTPD_PROMETHEUS_PORT}

CMD ["python", "-m", "rs_smtpd.server"]

