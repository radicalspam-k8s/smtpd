from logging import debug
from pprint import pprint
from flask import Flask, jsonify, request

app = Flask(__name__)
app.config['DEBUG'] = True

@app.route('/mail/receive', methods=['POST'])
def mail_receive():    
    data = request.json
    data['remote_addr'] = request.remote_addr
    print('----------- RECEIVE NEW MESSAGE --------------')
    pprint(data)
    print('-------------------- END ---------------------')
    return jsonify({"smtp_response": "250 Message accepted for delivery"})

    # curl -X POST "http://127.0.0.1:5000/mail/receive" -H "accept: application/json" -H "Content-Type: application/json" -d "{\"field\":\"test\"}"

if __name__ == "__main__":
    app.run()

